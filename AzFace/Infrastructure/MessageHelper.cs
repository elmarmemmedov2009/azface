﻿using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzFace.Infrastructure
{
    public class MessageHelper
    {
        public List<Message> Messages { get; set; }
        public Person Sender { get; set; }
        public Person Receiver { get; set; }
    }

    public class AlbumHelper
    {
        public int AlbumId;
    }
}