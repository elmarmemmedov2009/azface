﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AzFace.Infrastructure
{
    public class ResetPasswordViewModel
    {
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string Email { get; set; }
        public string code { get; set; }
        public string userId { get; set; }
    }
}