﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace AzFace.Infrastructure
{
    public class SiteLanguage 
    {
        public static List<Languages> availableLanguages = new List<Languages> 
        {
            new Languages {LangFullName="Azərbaycan", LangCultureName="Az"},
            new Languages {LangFullName="English", LangCultureName="En"}
        };

        public static bool IsLanguageAvailable(String lang)
        {
            return availableLanguages.Where(a => a.LangCultureName.Equals(lang)).FirstOrDefault() != null ? true : false;
        }

        public static string GetDefaultLanguage()
        {
            return availableLanguages[0].LangCultureName;
        }

        public void SetLanguage(String lang) 
        {
            try
            {
                if (!IsLanguageAvailable(lang))
                    lang = GetDefaultLanguage();
                var cultureInfo = new CultureInfo(lang);
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);
                HttpCookie langCookie = new HttpCookie("culture", lang);
                langCookie.Expires = DateTime.Now.AddYears(1); 
                HttpContext.Current.Response.Cookies.Add(langCookie); 

            }
            catch (Exception ex)
            { }
        }
    }

    public class Languages 
    {
        public string LangFullName { get; set; }
        public string LangCultureName { get; set; }

    }
}

