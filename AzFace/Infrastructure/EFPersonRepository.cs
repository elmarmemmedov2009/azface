﻿using AzFace.Models;
using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace AzFace.Infrastructure
{
    public class EFPersonRepository : IDisposable
    {
        void IDisposable.Dispose()
        { }

        public IdentityDbsEntities context = new IdentityDbsEntities();

        public IQueryable<Person> People
        {
            get { return context.People; }
        }

        public IQueryable<Photo> Photos
        {
            get { return context.Photos; }
        }
        public IQueryable<Item> Items
        {
            get { return context.Items; }
        }

        public IQueryable<Comment> Comments
        {
            get { return context.Comments; }
        }
        public IQueryable<Friend> Friends
        {
            get { return context.Friends; }
        }

        public IQueryable<Message> Messages
        {
            get { return context.Messages; }
        }

        public IQueryable<AspNetUser> AspNetUsers
        {
            get { return context.AspNetUsers; }
        }

        public IQueryable<ItemLike> ItemLikes
        {
            get { return context.ItemLikes; }
        }

        public IQueryable<Share> Shares
        {
            get { return context.Shares; }
        }

        public IQueryable<Album> Albums
        {
            get { return context.Albums; }
        }


        public void SavePerson(String firstName, String lastName, DateTime birthday, string userId)
        {
            Person person = new Person { FirstName = firstName, LastName = lastName, Birthday = birthday, UserId = userId, Status = "off" };

            if (person != null)
            {
                //if (person.Photos.Count == 0)
                //{
                //    String filePath = "nature.jpg";
                //    int fileSize = (int)new System.IO.FileInfo(filePath).Length;
                //    Image img = Image.FromFile (filePath);
                //    MemoryStream tmpStream = new MemoryStream();
                //    img.Save (tmpStream, ImageFormat.Jpeg);
                //    tmpStream.Seek (0, SeekOrigin.Begin);
                //    byte[] imgBytes = new byte[fileSize];
                //    tmpStream.Read (imgBytes, 0, fileSize);

                //    Photo photo = new Photo {Image = imgBytes, ImageType = "profile"};
                //    person.Photos.Add(photo);
                //}
                context.People.Add(person);
                context.SaveChanges();
            }
        }
        public void SaveComment(Comment comment)
        {
            context.Comments.Add(comment); // comments lere comment i elave edirem ve saveChanges edirem. Entity framework save edir.
            context.SaveChanges();       // bele, cox sade.
        }
        public void SaveItem(Item item)
        {
            context.Items.Add(item);
            context.SaveChanges();
        }

        public void RemoveItem(Item item)
        {
            context.Items.Remove(item);
            context.SaveChanges();
        }

        public void SaveFriend(Friend friend)
        {
            context.Friends.Add(friend);
            context.SaveChanges();
        }

        public void SaveMessage(Message message)
        {
            context.Messages.Add(message);
            context.SaveChanges();
        }

        public bool SaveItemLike(ItemLike itemLike)
        {
            try
            {
                context.ItemLikes.Add(itemLike);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                context.ItemLikes.Remove(itemLike);
                return false;
            }
        }

        public void SaveShare(Share share)
        {
            context.Shares.Add(share);
            context.SaveChanges();
        }
    }
}