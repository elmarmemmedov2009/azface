﻿using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzFace.Infrastructure
{
    public class CarouselItem
    {
        public int? PersonId;
        public string ImgMimeType;
        public string PersonName;
        public int ItemId;
        public string Text;
        public List<CarouselComment> Comments;
        public int? LikeNumber;
        public int? InitialPersonId;
        public string InitialPersonName;
        public string SharedTime;
        public string ShareOption;
        public string VideoPath;
        public int NextItemId;
        public int? NextPersonId;
    }

    public class CarouselComment
    {
        public int? PersonId;
        public string CommentTime;
        public string Text;
        public string FirstName;
        public string LastName;
    }
}