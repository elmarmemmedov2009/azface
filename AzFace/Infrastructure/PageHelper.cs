﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzFace.Infrastructure
{
    public class PageHelper
    {
        public int Page { get; set; }
        public int PersonId { get; set; }
    }
}