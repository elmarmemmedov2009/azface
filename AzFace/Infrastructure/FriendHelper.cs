﻿using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;

namespace AzFace.Infrastructure
{
    public static class FriendHelper
    {
        public static bool IsFriend(int currentPersonId, int PersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                List<Friend> friends = db.Friends.Where(x => (
                    (x.PersonId1 == currentPersonId && x.PersonId2 == PersonId) ||
                    (x.PersonId1 == PersonId && x.PersonId2 == currentPersonId)
                    ) &&
                    x.Status == "1").ToList();

                if (friends.Count() > 0)
                    return true;
                else
                {
                    return false;
                }

            }
        }



        public static object GetFriendRequestes(int PersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                List<Friend> friendRequestes = db.Friends.Where(x => x.PersonId2 == PersonId && x.Status == "0").ToList();

                var requestDetails = from person1 in friendRequestes
                                     join person2 in db.People
                                     on person1.PersonId1 equals person2.PersonId
                                     select new { FriendId = person1.FriendId, PersonId = person2.PersonId, FirstName = person2.FirstName, LastName = person2.LastName }.ToExpando();

                return requestDetails;
            }
        }

        public static int GetFriendRequestCount(int PersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                List<Friend> friendRequestes = db.Friends.Where(x => x.PersonId2 == PersonId && x.Status == "0").ToList();
                return friendRequestes.Count();
            }
        }

        public static object GetMessages(int PersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                List<Message> newMessages = db.Messages.Where(x => x.MessageTo == PersonId && x.ReadTimes < 2).ToList();
                var messageDetails = from person1 in newMessages
                                     join person2 in db.People
                                     on person1.MessageFrom equals person2.PersonId

                                     select new
                                     {
                                         MessageId = person1.MessageId,
                                         PersonId = person1.MessageFrom,
                                         FirstName = person2.FirstName,
                                         LastName = person2.LastName,
                                         Text = person1.Text
                                     }.ToExpando();
                return (messageDetails);
            }
        }

        public static int GetMessageCount(int PersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                List<Message> newMessages = db.Messages.Where(x => x.MessageTo == PersonId && x.ReadTimes < 2).ToList();
                return newMessages.Count();
            }
        }

        public static object MostMessaged(int personId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                var messages = db.Messages.Where(x =>
                    x.MessageTo == personId).GroupBy(y => y.MessageFrom).Select(n => n.FirstOrDefault()).OrderByDescending(m => m.MessageTime).Take(10).ToList();

                var friends = FriendsList(personId);


                var messageDetails = from message in messages
                                     join person in friends
                                     on message.MessageFrom equals person.PersonId

                                     select new
                                     {
                                         PersonId = message.MessageFrom,
                                         FirstName = person.FirstName,
                                         LastName = person.LastName,
                                         Status = person.Status
                                     }.ToExpando();
                return (messageDetails);
            }
        }

        public static List<Person> FriendsList(int personId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                var friends = db.People.Where(x => x.Friends.Any(y => (y.PersonId2 == personId) && y.Status == "1"));
                var friends1 = db.People.Where(x => x.Friends1.Any(y => (y.PersonId1 == personId) && y.Status == "1"));

                List<Person> friends3 = friends.Union(friends1).ToList();
                return friends3;
            }
        }



        public static string CreateUserName(string firstName, string lastName)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int userCount = db.People.Where(x => x.FirstName.ToLower() == firstName.ToLower() && x.LastName.ToLower() == lastName.ToLower()).Count();
                string userName = firstName + lastName + (userCount + 1);
                return userName;
            }
        }



    }

  
}