﻿using AzFace.Infrastructure;
using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AzFace.Controllers
{


    public class ItemTest
    {
        public int? PersonId2;
        public Photo ImgMimeType;
        public string PersonId;
        public int ItemId;
        public string Text;
        public List<string> Comments;
        public int? LikeNumber;
        public int? InitialPersonId;
        public string InitialPersonName;
        public string SharedTime;
        public string ShareOption;
        public string VideoPath;
    }



    public class PartialItemController : Controller
    {
        //
        // GET: /PartialItem/

        public PartialViewResult Index(PageHelper ph) 
        {

            using (EFPersonRepository db = new EFPersonRepository())
            {

                Thread.Sleep(1000);

                int personId = Convert.ToInt32(Session["PersonId"]);

                int page = ph.Page;
                int pageSize = 1;

                if (page == 1) pageSize = 2; 

                List<Item> items1 = db.Items.Where(n => n.PersonId == personId || n.ShareOption == "Public" || (n.ShareOption != "OnlyMe" && n.Person.Friends.Any(y => (y.PersonId2 == personId) && y.Status == "1"))).ToList();
                List<Item> items2 = db.Items.Where(n => n.PersonId == personId || n.ShareOption == "Public" || (n.ShareOption != "OnlyMe" && n.Person.Friends1.Any(y => (y.PersonId1 == personId) && y.Status == "1"))).ToList();

                List<Item> items3 = items1.Union(items2).ToList();

                List<Item> items = items3.OrderByDescending(x => x.SharedTime).Skip((page - 1) * pageSize).Take(pageSize).ToList();

                List<ItemTest> item = items.Select(n => new ItemTest
                {
                    ImgMimeType = n.Photos.FirstOrDefault(),
                    PersonId2 = n.PersonId,
                    PersonId = n.Person.FirstName + " " + n.Person.LastName,
                    ItemId = n.ItemId,
                    Text = n.Text,
                    Comments = n.Comments.Select(x => x.Person.FirstName + " at " + x.CommentTime + ":    " + x.Text).ToList(),
                    LikeNumber = n.LikeNumber,
                    InitialPersonId = n.InitialPersonId,
                    InitialPersonName = n.Person1.FirstName + " " + n.Person1.LastName,
                    SharedTime = n.SharedTime.ToString(),
                    ShareOption = n.ShareOption,
                    VideoPath = n.VideoPath
                }).ToList();
                return PartialView(item);
            }

            
        }

        public PartialViewResult GetItemsToProfile(PageHelper ph)
        {

            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = Convert.ToInt32(Session["PersonId"]);


                int page = ph.Page;
                int pageSize = 1;

                if (page == 1) pageSize = 2;


                List<Item> items1 = db.Items.Where(n => n.PersonId == personId).ToList();

                List<Item> items = items1.OrderByDescending(x => x.SharedTime).Skip((page - 1) * pageSize).Take(pageSize).ToList();

                List<ItemTest> item = items.Select(n => new ItemTest
                {
                    PersonId2 = n.PersonId,
                    ImgMimeType = n.Photos.FirstOrDefault(),
                    PersonId = n.Person.FirstName + " " + n.Person.LastName,
                    ItemId = n.ItemId,
                    Text = n.Text,
                    Comments = n.Comments.Select(x => x.Person.FirstName + " at " + x.CommentTime + ":    " + x.Text).ToList(),
                    LikeNumber = n.LikeNumber,
                    InitialPersonId = n.InitialPersonId,
                    InitialPersonName = n.Person1.FirstName + " " + n.Person1.LastName,
                    SharedTime = n.SharedTime.ToString(),
                    ShareOption = n.ShareOption,
                    VideoPath = n.VideoPath
                }).ToList();
                return PartialView("Index",item);
            }


        }

    }
}
