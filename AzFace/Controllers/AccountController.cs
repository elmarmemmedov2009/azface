﻿using AzFace.Infrastructure;
using AzFace.Models;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using System.Net.Mail;
using AzFace.Models.Connection;
using System.Threading;


namespace AzFace.Controllers
{

    public class AccountController : MyBaseController
    {

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Create(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Create(CreateModel model, string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            if (ModelState.IsValid)
            {

                String userName = FriendHelper.CreateUserName(model.FirstName, model.LastName);

                AppUser user = new AppUser { UserName = userName, Email = model.Email };
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    AppUser user1 = UserManager.FindByName(userName);
                    using (EFPersonRepository db = new EFPersonRepository())
                    {
                        db.SavePerson(model.FirstName, model.LastName, model.Birthday, user1.Id);
                    }
                    TempData["Success"] = "Your account is created";
                    return RedirectToAction("Login");
                }
                else
                {
                    AddErrorsFromResult(result);
                }
            }
            return View(model);
        }


        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return View("Error", new string[] { "Access Denied" });
            }
            if (returnUrl == null)
                ViewBag.returnUrl = "/";
            if (TempData["Success"] != null)
            {
                ViewBag.SuccessMessage = TempData["Success"];
            }



            return View();
        }



        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginModel details) // gelir bura, amma burda Email , password yoxdu. Model binding bas verir burda
        {
            if (ModelState.IsValid)
            {
                string userName = "1";
                AppUser user1 = await UserManager.FindByEmailAsync(details.Email);
                if (user1 != null)
                    userName = user1.UserName;

                AppUser user = await UserManager.FindAsync(userName, details.Password);
                if (user == null)
                {
                    ModelState.AddModelError("", "Invalid name or password.");
                }
                else
                {
                    ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthManager.SignOut();
                    AuthManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = false
                    }, ident);
                    return Redirect("/");
                }
            }
            return View(details);
        }


        public ActionResult ChangeLanguage(string lang)
        {
            new SiteLanguage().SetLanguage(lang);
            return RedirectToAction("Create", "Account");
        }




        [Authorize]
        public ActionResult Logout()
        {
            AuthManager.SignOut();
            return RedirectToAction("Index", "Home");
        }



        public ActionResult ForgotPassword()
        {
            ForgotPasswordViewModel model = new ForgotPasswordViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            //    var user = await UserManager.FindByEmailAsync(model.Email);
            //    if (user == null)
            //    {
            //        // Don't reveal that the user does not exist or is not confirmed
            //        return View("ForgotPasswordConfirmation");
            //    }

            //    // Send an email with this link
            //    var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            //    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            //    await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
            //    return RedirectToAction("Login", "Account");
            //}

            //// If we got this far, something failed, redisplay form
            //return View("Login");

            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    Random r = new Random();
                    Thread.Sleep(r.Next(1851, 2949));
                    return RedirectToAction("Sent");
                }

                // Send an email with this link
                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                using (EFPersonRepository db = new EFPersonRepository())
                {
                    AspNetUser user1 = db.AspNetUsers.FirstOrDefault(x => x.Id == user.Id);
                    user1.PasswordResetTokenExpire = DateTime.Now.AddDays(1);
                    db.context.SaveChanges();
                }

                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                var message = new MailMessage();
                message.To.Add(new MailAddress(model.Email));
                message.From = new MailAddress("elmarmemmedov2009@gmail.com");
                message.Subject = "Password Reset Link";
                message.Body = "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a><br><p>This link is valid for 1 day</p><br><p>If you did not send this email, change your password as soon as possible</p>";
                message.IsBodyHtml = true;
                //message.Attachments.Add(new Attachment("C:\\Users\\elmar.mammadov\\Desktop\\HomeIndex.txt"));

                using (var smtp = new SmtpClient())
                {

                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Sent");
                }
            }
            return View(model);
        }

        public ActionResult Sent()
        {
            return View();
        }


        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                AspNetUser user = db.AspNetUsers.FirstOrDefault(x => x.Id == model.userId);

                if (user.PasswordResetToken.ToString() == model.code)
                {
                    return View(model);
                }
                return null;
            }


        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmPassword(ResetPasswordViewModel model)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                AspNetUser user = db.AspNetUsers.FirstOrDefault(x => x.Id == model.userId);
                
                if (user != null && user.PasswordResetToken.ToString() == model.code && user.PasswordResetTokenExpire > DateTime.Now)
                {
                    if (model.Password.Any(x => char.IsDigit(x)) && model.Password.Any(y => char.IsUpper(y)) && model.Password.Count() > 5)
                    {
                        UserManager.ResetPassword(model.userId, model.code, model.Password);
                        user.PasswordResetTokenExpire = DateTime.Now;
                        db.context.SaveChanges();

                        return View("PasswordReset");
                    }
                    else
                    {
                        return View("PasswordNotReset");
                    }

                }
                return View("LinkExpired");
            }
        }

        public ActionResult PasswordReset()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult GoogleLogin(string returnUrl = "/")
        {
            var properties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("GoogleLoginCallback",
                new { returnUrl = returnUrl })
            };
            HttpContext.GetOwinContext().Authentication.Challenge(properties, "Google");
            return new HttpUnauthorizedResult();
        }
        [AllowAnonymous]
        public async Task<ActionResult> GoogleLoginCallback(string returnUrl)
        {
            ExternalLoginInfo loginInfo = await AuthManager.GetExternalLoginInfoAsync();
            AppUser user = await UserManager.FindAsync(loginInfo.Login);
            if (user == null)
            {
                user = new AppUser
                {
                    Email = loginInfo.Email,
                    UserName = loginInfo.DefaultUserName
                    //City = "Baku", Country = Countries.Azerbaijan
                };
                IdentityResult result = await UserManager.CreateAsync(user);
                if (!result.Succeeded)
                {
                    return View("Error", result.Errors);
                }
                else
                {
                    result = await UserManager.AddLoginAsync(user.Id, loginInfo.Login);
                    if (!result.Succeeded)
                    {
                        return View("Error", result.Errors);
                    }
                }
            }
            ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user,
            DefaultAuthenticationTypes.ApplicationCookie);
            ident.AddClaims(loginInfo.ExternalIdentity.Claims);
            AuthManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = false
            }, ident);
            return Redirect(returnUrl ?? "/");
        }












        private IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}
