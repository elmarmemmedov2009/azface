﻿using AzFace.Infrastructure;
using AzFace.Models;
using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;



namespace AzFace.Controllers
{
    public class ProfileController : Controller
    {

        public ActionResult Upload(ProfileViewModel model)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = Convert.ToInt32(Session["PersonId"]);

                ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(personId);
                ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(personId);
                ViewBag.MessageCount = FriendHelper.GetMessageCount(personId);
                ViewBag.Messages = FriendHelper.GetMessages(personId);



                var image = WebImage.GetImageFromRequest();

                if (image != null)
                {
                    if (image.Width > 500)
                    {
                        image.Resize(500, ((500 * image.Height) / image.Width));
                    }

                    var filename = Path.GetFileName(image.FileName);
                    image.Save(Path.Combine("~/Images", filename));
                    filename = Path.Combine("~/Images", filename);
                    model.ImageUrl = Url.Content(filename);
                    var editModel = new EditorInputModel()
                       {
                           Profile = model,
                           Width = image.Width,
                           Height = image.Height,
                           Top = image.Height * 0.1,
                           Left = image.Width * 0.9,
                           Right = image.Width * 0.9,
                           Bottom = image.Height * 0.9
                       };
                    return View("Editor", editModel);

                }

                return View("Index", model);
            }
        }

        public ActionResult Edit(EditorInputModel editor)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = Convert.ToInt32(Session["PersonId"]);

                ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(personId);
                ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(personId);
                ViewBag.MessageCount = FriendHelper.GetMessageCount(personId);
                ViewBag.Messages = FriendHelper.GetMessages(personId);

                var image = new WebImage("~" + editor.Profile.ImageUrl);
                var height = image.Height;
                var width = image.Width;
                image.Crop((int)editor.Top, (int)editor.Left, (int)(height - editor.Bottom), (int)(width - editor.Right));
                var originalFile = editor.Profile.ImageUrl;
                editor.Profile.ImageUrl = Url.Content("~/ProfileImages/" + Path.GetFileName(image.FileName));
                image.Resize(140, 140, true, false);
                image.Save(@"~" + editor.Profile.ImageUrl);

                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                Photo photo = new Photo
                {
                    Person = person,
                    ImageMimeType = image.ImageFormat,
                    ImageType = "profile"
                };
                photo.Image = image.GetBytes();

                person.Photos.Add(photo);
                db.context.SaveChanges();




                System.IO.File.Delete(Server.MapPath(originalFile));
                return RedirectToAction("Index", "Home", null);
            }
        }


        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

    }
}

