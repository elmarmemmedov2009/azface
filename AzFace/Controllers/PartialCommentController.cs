﻿using AzFace.Infrastructure;
using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AzFace.Controllers
{

    public class ComRep
    {
        public int CommentReplyId;
        public int? CommentId;
        public int? ReplyId;
        public string Text;
        public int? PersonId;
        public string PersonName;
    }

    public class Com
    {
        public int CommentId;
        public string Text;
        public DateTime? CommentTime;
        public int? PersonId;
        public string FirstName;
        public string LastName;
        public List<ComRep> CommentReplys;
    }

    public class Test
    {
        public int ItemId;
        public List<Com> Comments;
        public int? PersonId;
        public DateTime? SharedTime;
        public string Origin;
        public string PersonName;
        public string Text;
    }

    public class PartialCommentController : Controller
    {

        [HttpPost]
        public PartialViewResult Index(int id, string origin, int page = 1)
        {
            int personId = Convert.ToInt32(Session["PersonId"]);
            ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(personId);
            ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(personId);
            ViewBag.MessageCount = FriendHelper.GetMessageCount(personId);
            ViewBag.Messages = FriendHelper.GetMessages(personId);
            ViewBag.MostMessaged = FriendHelper.MostMessaged(personId);

            ViewBag.Page = page;
            int pageSize = 8;

            using (EFPersonRepository db = new EFPersonRepository())
            {
                Item item = db.Items.FirstOrDefault(x => x.ItemId == id);

                var item2 = new Test
                {
                    ItemId = item.ItemId,
                    Comments = item.Comments.Select(x => new Com
                    {
                        CommentId = x.CommentId,
                        Text = x.Text,
                        CommentTime = x.CommentTime,
                        PersonId = x.PersonId,
                        FirstName = x.Person.FirstName,
                        LastName = x.Person.LastName,
                        CommentReplys = x.CommentReplies.Select(y => new ComRep
                        {
                            CommentReplyId = y.ComentReplyId,
                            CommentId = y.CommentId,
                            ReplyId = y.ReplyId,
                            Text = y.Comment1.Text,
                            PersonId = y.Comment1.PersonId,
                            PersonName = y.Comment1.Person.FirstName + " " + y.Comment1.Person.LastName
                        }).ToList()
                    }).OrderByDescending(y => y.CommentTime).Skip((page - 1) * pageSize).Take(pageSize).ToList(),
                    PersonId = item.PersonId,
                    PersonName = item.Person.FirstName+" "+ item.Person.LastName,
                    SharedTime = item.SharedTime,
                    Origin = origin,
                    Text = item.Text
                };

                ViewBag.Id = id;
                return PartialView(item2);
            }

        }




    }
}
