﻿using AzFace.Infrastructure;
using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AzFace.Models;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using System.Dynamic;
using System.ComponentModel;
using System.IO;



namespace AzFace.Controllers
{
    

    public class HomeController : MyBaseController
    {
        [Authorize]
        public ActionResult Index()
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                String name = User.Identity.Name;
                Session["UserName"] = name;
                AppUser user = UserManager.FindByName(name);
                String userId = user.Id;
                ViewBag.UserName = name;
                Person person = db.People.FirstOrDefault(x => x.UserId == userId);
                ViewBag.PersonName = person.FirstName;
                Session["PersonId"] = person.PersonId;

                ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(person.PersonId);
                ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(person.PersonId);
                ViewBag.MessageCount = FriendHelper.GetMessageCount(person.PersonId);
                ViewBag.Messages = FriendHelper.GetMessages(person.PersonId);
                ViewBag.MostMessaged = FriendHelper.MostMessaged(person.PersonId);

                ProfileViewModel model = new ProfileViewModel();
                model.person = person;
                return View(model);
            }
        }

        [Authorize]
        public ActionResult Home()
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                String name = User.Identity.Name;
                Session["UserName"] = name;
                AppUser user = UserManager.FindByName(name);
                String userId = user.Id;
                ViewBag.UserName = name;
                Person person = db.People.FirstOrDefault(x => x.UserId == userId);
                ViewBag.PersonName = person.FirstName;
                Session["PersonId"] = person.PersonId;

                ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(person.PersonId);
                ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(person.PersonId);
                ViewBag.MessageCount = FriendHelper.GetMessageCount(person.PersonId);
                ViewBag.Messages = FriendHelper.GetMessages(person.PersonId);
                ViewBag.MostMessaged = FriendHelper.MostMessaged(person.PersonId);
                return View(person);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult UploadProfileImage(HttpPostedFileBase image)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {

                int personId = Convert.ToInt32(Session["PersonId"]);
                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                Photo photo = new Photo
                {
                    Person = person,
                    ImageMimeType = image.ContentType,
                    ImageType = "profile"
                };

                photo.Image = new byte[image.ContentLength];
                image.InputStream.Read(photo.Image, 0, image.ContentLength);

                person.Photos.Add(photo);
                db.context.SaveChanges();

                return RedirectToAction("Index");
            }
        }

        [Authorize]
        //[ValidateAntiForgeryToken]
        public JsonResult GetPeopleDataJson(String searchTerm)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                if (!String.IsNullOrWhiteSpace(searchTerm))
                {
                    int personId = Convert.ToInt32(Session["PersonId"]);
                    List<Person> people = db.People.Where(x => x.FirstName.StartsWith(searchTerm) || x.LastName.StartsWith(searchTerm)).ToList();
                    var people1 = people.Where(x => x.PersonId != personId).Select(p =>
                        new { UserName = p.AspNetUser.UserName, PersonId = p.PersonId, LastName = p.LastName, FirstName = p.FirstName }).ToList();

                    return Json(people1, JsonRequestBehavior.AllowGet);
                }
                else
                    return null;
            }
        }
        [Authorize]
        public JsonResult SharePeopleDataJson(String shareTerm)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = Convert.ToInt32(Session["PersonId"]);
                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                Item item = new Item { Text = shareTerm, SharedTime = DateTime.Now, Person = person };
                db.SaveItem(item);
                return Json("Item shared on your timeline", JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult ShareItem(Item item, HttpPostedFileBase image)
        {
            int personId = Convert.ToInt32(Session["PersonId"]);
            String ext = null;
            if (image != null)
            {
                ext = System.IO.Path.GetExtension(image.FileName);
            }
            if (image != null)
            {


                if (ext.Equals(".mp4"))
                {
                    if (image.ContentLength > 0)
                    {

                        string subPath = "~/VideoFile/" + personId;
                        bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));
                        if (!exists)
                            System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                        string filePath = Path.Combine(Server.MapPath(subPath), Path.GetFileName(image.FileName));
                        image.SaveAs(filePath);

                        using (EFPersonRepository db = new EFPersonRepository())
                        {
                            Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                            item.Person = person;
                            item.SharedTime = DateTime.Now;
                            item.LikeNumber = 0;
                            item.InitialPersonId = personId;
                            item.VideoPath = Path.GetFileName(image.FileName);
                            Share share = new Share
                            {
                                ItemId = item.ItemId,
                                PersonId = personId
                            };
                            db.context.Shares.Add(share);
                            db.SaveItem(item);



                            return RedirectToAction("Index");
                        }
                    }

                }
            }

            using (EFPersonRepository db = new EFPersonRepository())
            {
                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                item.Person = person;
                item.SharedTime = DateTime.Now;
                item.LikeNumber = 0;
                item.InitialPersonId = personId;

                Share share = new Share
                {
                    ItemId = item.ItemId,
                    PersonId = personId
                };
                db.context.Shares.Add(share);
                if (image != null && ext.Equals(".jpg"))
                {
                    //item.ImageMimeType = image.ContentType;
                    //item.ImageData = new byte[image.ContentLength];
                    //image.InputStream.Read(item.ImageData, 0, image.ContentLength);

                    Photo photo = new Photo
                    {
                        ItemId = item.ItemId,
                        PersonId = personId,
                        ImageMimeType = image.ContentType,
                        Image = new byte[image.ContentLength]
                    };
                    image.InputStream.Read(photo.Image, 0, image.ContentLength);

                    item.Photos.Add(photo);
                }
                db.SaveItem(item);



                return RedirectToAction("Index");
            }

        }
        [Authorize]
        [HttpPost]
        public JsonResult ChangeItem(int itemId, string option)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = Convert.ToInt32(Session["PersonId"]);
                Item item = db.Items.FirstOrDefault(x => x.ItemId == itemId);
                if (item.PersonId != personId)
                {
                    return null;
                }

                if (option != "Remove")
                {
                    item.ShareOption = option;
                    db.context.SaveChanges();
                    return Json(new { ItemOption = option, ItemId = itemId }, JsonRequestBehavior.AllowGet);
                }
                else if (option == "Remove")
                {
                    db.RemoveItem(item);
                }

                return Json(new { ItemOption = option, ItemId = itemId }, JsonRequestBehavior.AllowGet);
            }
        }


        [Authorize]
        [HttpPost]
        public ActionResult ShareOthersItem(int itemId, string shareOption1, string Text)
        {

            using (EFPersonRepository db = new EFPersonRepository())
            {

                int personId = Convert.ToInt32(Session["PersonId"]);
                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                Item item = db.Items.FirstOrDefault(x => x.ItemId == itemId);
                Share share = db.Shares.FirstOrDefault(x => x.ItemId == itemId);
                if (share != null)
                {
                    item.InitialPersonId = share.PersonId;
                }
                List<Comment> comments = new List<Comment>();
                List<ItemLike> itemLikes = new List<ItemLike>();

                for (int i = 0; i < item.Comments.Count(); i++)
                {
                    Comment comment = item.Comments.ElementAt(i);
                    Comment comment1 = new Comment
                    {
                        CommentTime = comment.CommentTime,
                        PersonId = comment.PersonId,
                        Text = comment.Text
                    };
                    comments.Add(comment1);
                }

                for (int i = 0; i < item.ItemLikes.Count(); i++)
                {
                    ItemLike itemLike = item.ItemLikes.ElementAt(i);
                    itemLikes.Add(itemLike);
                }

                Item newItem = new Item
                {
                    Comments = comments.ToList(),
                    ImageData = item.ImageData,
                    ImageMimeType = item.ImageMimeType,
                    ItemLikes = item.ItemLikes.ToList(),
                    LikeNumber = item.LikeNumber,
                    SharedTime = DateTime.Now,
                    ShareOption = shareOption1,
                    Text = item.Text + "\n" + Text,
                    Person = person,
                    InitialPersonId = item.InitialPersonId
                };


                db.SaveItem(newItem);
                return RedirectToAction("Index");
            }
        }












        //This method is used to get first item object to friend's profile
        [Authorize]
        [HttpPost]
        public JsonResult GetFriendProfile1(int PersonId, int page = 1)
        {        
            using (EFPersonRepository db = new EFPersonRepository())
            {   
                int pageSize = 1;
                List<Item> items = db.Items.Where(n => n.PersonId == PersonId && n.ShareOption == "Public").OrderByDescending(x => x.SharedTime).
                    Skip((page - 1) * pageSize).Take(pageSize).ToList();

                var item = items.Select(n => new
                {
                    PersonName = n.Person.FirstName + " " + n.Person.LastName,
                    PersonId = n.PersonId,
                    ImgMimeType = n.ImageMimeType,
                    ItemId = n.ItemId,
                    Text = n.Text,
                    Comments = n.Comments.Select(x => "<span style='color:blue'>" + x.Person.FirstName + " at " + x.CommentTime + " </span> " + x.Text),
                    LikeNumber = n.LikeNumber,
                    InitialPersonId = n.InitialPersonId,
                    InitialPersonName = n.Person1.FirstName + " " + n.Person1.LastName
                });
                return Json(item, JsonRequestBehavior.AllowGet);
            }
        }


        //This method is used to get all item objects of friend's profile except first
        [Authorize]
        [HttpPost]
        public JsonResult GetFriendProfile(PageHelper ph)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = ph.PersonId;

                int page = ph.Page;
                int pageSize = 1;
                List<Item> items1 = db.Items.Where(n => n.PersonId == personId && n.ShareOption == "Public").ToList();

                List<Item> items = items1.OrderByDescending(x => x.SharedTime).Skip((page - 1) * pageSize).Take(pageSize).ToList();

                var item = items.Select(n => new
                {
                    PersonId2 = n.PersonId,
                    ImgMimeType = n.ImageMimeType,
                    PersonId = n.Person.FirstName + " " + n.Person.LastName,
                    ItemId = n.ItemId,
                    Text = n.Text,
                    Comments = n.Comments.Select(x => x.Person.FirstName + " at " + x.CommentTime + ":    " + x.Text),
                    LikeNumber = n.LikeNumber
                });
                return Json(item, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult AddFriend(int PersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = Convert.ToInt32(Session["PersonId"]);
                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                Person friendPerson = db.People.FirstOrDefault(x => x.PersonId == PersonId);
                Friend friend = new Friend { Person = person, Person1 = friendPerson, Status = "0" };
                db.SaveFriend(friend);
                return Json("friend request sent", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ConfirmFriendship(Nullable<int> str)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                Friend friend = db.Friends.FirstOrDefault(x => x.FriendId == str);
                friend.Status = "1";
                db.context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult GetFriends()
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = Convert.ToInt32(Session["PersonId"]);

                ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(personId);
                ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(personId);
                ViewBag.MessageCount = FriendHelper.GetMessageCount(personId);
                ViewBag.Messages = FriendHelper.GetMessages(personId);

                Person person = db.People.FirstOrDefault(p => p.PersonId == personId);
                ViewBag.PersonName = person.FirstName;

                List<Person> friends = FriendHelper.FriendsList(personId);

                return View(friends);
            }
        }

        [Authorize]
        public JsonResult AddCommentToItem(int itemId, string comment)
        {
            if (itemId == 0 || String.IsNullOrWhiteSpace(comment) || String.IsNullOrEmpty(comment))
            {
                return null;
            }
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = Convert.ToInt32(Session["PersonId"]);
                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                Item item = db.Items.FirstOrDefault(x => x.ItemId == itemId);

                Comment itemComment = new Comment
                {
                    Text = comment,
                    CommentTime = DateTime.Now, 
                    Person = person,
                    ItemId = itemId,
                    Item = item
                };

                db.SaveComment(itemComment);

                var itemAndText = new { ItemId = itemId, ItemComment = comment };

                return Json(itemAndText, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetCommentsFromItem(int itemId, string origin, int page = 1)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                Item item = db.Items.FirstOrDefault(x => x.ItemId == itemId);

                var item2 = new
                {
                    ItemId = item.ItemId,
                    Page = page,
                    Origin = origin
                };

                return Json(item2, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult EditComment(Comment comment)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int currentPersonId = Convert.ToInt32(Session["PersonId"]);

                Comment newComment = db.Comments.FirstOrDefault(x => x.CommentId == comment.CommentId);
                if (currentPersonId != newComment.PersonId)
                {
                    return null;
                }
                newComment.Text = comment.Text;
                db.context.SaveChanges();
                return Json(new { CommentId = comment.CommentId, Text = comment.Text }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult DeleteComment(Comment comment)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int currentPersonId = Convert.ToInt32(Session["PersonId"]);

                Comment newComment = db.Comments.FirstOrDefault(x => x.CommentId == comment.CommentId);
                if (currentPersonId != newComment.PersonId)
                {
                    return null;
                }
                db.context.Comments.Remove(newComment);
                db.context.SaveChanges();
                return Json(new { CommentId = comment.CommentId, Text = comment.Text }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult ReplyToComment(Comment comment)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int currentPersonId = Convert.ToInt32(Session["PersonId"]);

                Comment newComment = db.Comments.FirstOrDefault(x => x.CommentId == comment.CommentId);
                int? itemId = newComment.ItemId;
                Comment newComment1 = new Comment { PersonId = currentPersonId, Text = comment.Text };
                CommentReply commentReply = new CommentReply { Comment = newComment, Comment1 = newComment1 };

                newComment.CommentReplies.Add(commentReply);
                db.context.SaveChanges();
                return Json(new { ItemId = itemId }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult LikeItem(int itemId, int personId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                Item item = db.Items.FirstOrDefault(x => x.ItemId == itemId);

                ItemLike itemLike = new ItemLike { ItemId = itemId, PersonId = personId };
                bool isSaved = db.SaveItemLike(itemLike);

                if (isSaved)
                {
                    item.LikeNumber += 1;
                    db.context.SaveChanges();
                    var item2 = new
                    {
                        ItemId = itemId,
                        LikeNumber = item.LikeNumber
                    };
                    return Json(item2, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ItemLike itemLike2 = db.ItemLikes.FirstOrDefault(x => x.PersonId == personId && x.ItemId == itemId);
                    db.context.ItemLikes.Remove(itemLike2);
                    db.context.SaveChanges();

                    item.LikeNumber -= 1;
                    db.context.SaveChanges();

                    var item2 = new
                    {
                        ItemId = itemId,
                        LikeNumber = item.LikeNumber
                    };
                    return Json(item2, JsonRequestBehavior.AllowGet);
                }



            }
        }


       
        public ActionResult GetProfile(string userName)
        {
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            using (EFPersonRepository db = new EFPersonRepository())
            {
                ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(currentPersonId);
                ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(currentPersonId);
                ViewBag.Messages = FriendHelper.GetMessages(currentPersonId);
                ViewBag.MessageCount = FriendHelper.GetMessageCount(currentPersonId);


                Person person = db.AspNetUsers.FirstOrDefault(x => x.UserName == userName).People.FirstOrDefault();
                if (User.Identity.IsAuthenticated)
                {
                    Person person1 = db.People.FirstOrDefault(x => x.PersonId == currentPersonId);
                    ViewBag.PersonName = person1.FirstName;
                }
                

                bool isFriend = FriendHelper.IsFriend(currentPersonId, person.PersonId);
                ViewBag.IsFriendWith = isFriend;
                return View(person);
            }
        }


        //[Authorize]
        [HttpGet]
        public FileContentResult GetImage(int PersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                Person person = db.People.FirstOrDefault(p => p.PersonId == PersonId);
                Photo photo = (Photo)person.Photos.LastOrDefault(x => (x.PersonId == person.PersonId && x.ImageType == "profile"));
                if (photo != null && photo.Image != null)
                {
                    return File(photo.Image, photo.ImageMimeType);
                }
                else
                {
                    return null;
                }
            }
        }

        //[Authorize]
        [HttpGet]
        public FileContentResult GetWallpaper(int PersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                Person person = db.People.FirstOrDefault(p => p.PersonId == PersonId);
                Photo photo = (Photo)person.Photos.LastOrDefault(x => (x.PersonId == person.PersonId && x.ImageType == "wallpaper"));
                if (photo != null && photo.Image != null)
                {
                    return File(photo.Image, photo.ImageMimeType);
                }
                else
                {
                    return null;
                }
            }
        }


        [Authorize]
        [HttpPost]
        public ActionResult UploadWallPaper(HttpPostedFileBase image)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int personId = Convert.ToInt32(Session["PersonId"]);
                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                Photo photo = new Photo
                {
                    Person = person,
                    ImageMimeType = image.ContentType,
                    ImageType = "wallpaper"
                };

                photo.Image = new byte[image.ContentLength];
                image.InputStream.Read(photo.Image, 0, image.ContentLength);

                person.Photos.Add(photo);
                db.context.SaveChanges();

                return RedirectToAction("Index");
            }
        }

        [Authorize]
        [HttpGet]
        public FileContentResult GetSharedImage(int ItemId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                Item item = db.Items.FirstOrDefault(p => p.ItemId == ItemId);
                if (item.Photos.FirstOrDefault() != null)
                {
                    return File(item.Photos.ElementAt(0).Image, item.Photos.ElementAt(0).ImageMimeType);
                }
                else
                {
                    return null;
                }
            }
        }


        [Authorize]
        [HttpPost]
        public JsonResult GetCarouselItems(int personId, int itemId, string direction)
        {

            if (personId == 0 || itemId == 0)
                return null;

            using (EFPersonRepository db = new EFPersonRepository())
            {
                List<Item> items = db.Items.Where(x => x.ItemId <= itemId && x.Person.PersonId == personId && x.Photos.FirstOrDefault().ImageMimeType != null).OrderByDescending(n => n.ItemId).ToList();
                if (direction == "left")
                    items = db.Items.Where(x => x.ItemId > itemId && x.Person.PersonId == personId && x.Photos.FirstOrDefault().ImageMimeType != null).OrderBy(n => n.ItemId).ToList();
                Item item = null;
                int nextItemId = 0;
                int? nextPersonId = 0;

                if (items.Count() > 0)
                    item = items[0];
                if (items.Count() > 1)
                {
                    nextItemId = items[1].ItemId;
                    nextPersonId = items[1].PersonId;
                }




                var carouselItem = new
                {
                    PersonId = item.PersonId,
                    ImgMimeType = item.Photos.ElementAt(0).ImageMimeType,
                    PersonName = item.Person.FirstName + " " + item.Person.LastName,
                    ItemId = item.ItemId,
                    Text = item.Text,
                    Comments = item.Comments.Select(x => new { 
                        PersonId = x.PersonId, 
                        CommentTime = x.CommentTime.Value.ToShortDateString(), 
                        Text = x.Text, 
                        FirstName = x.Person.FirstName, 
                        LastName = x.Person.LastName }),
                    LikeNumber = item.LikeNumber,
                    InitialPersonId = item.InitialPersonId,
                    InitialPersonName = item.Person1.FirstName + " " + item.Person1.LastName,
                    SharedTime = item.SharedTime.ToString(),
                    ShareOption = item.ShareOption,
                    VideoPath = item.VideoPath,
                    NextItemId = nextItemId,
                    NextPersonId = nextPersonId
                };
                return Json(carouselItem, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetItemText(int itemId)
        {

            if (itemId == 0)
                return null;

            using (EFPersonRepository db = new EFPersonRepository())
            {
                Item item = db.Items.FirstOrDefault(x => x.ItemId == itemId);
                return Json(new { Text = item.Text, ItemId = item.ItemId }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetFriendsList(string friendName)
        {

    

            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            using (EFPersonRepository db = new EFPersonRepository())
            {
                var friends = db.People.Where(x => x.Friends.Any(y => (y.PersonId2 == currentPersonId) && y.Status == "1"));
                var friends1 = db.People.Where(x => x.Friends1.Any(y => (y.PersonId1 == currentPersonId) && y.Status == "1"));
                List<Person> friends3 = friends.Union(friends1).ToList();

                var searchedFriends = friends3.Where(x => x.FirstName.ToLower().StartsWith(friendName.ToLower()) || x.LastName.ToLower().StartsWith(friendName.ToLower())).Select(y => 
                    new {
                    PersonId = y.PersonId,
                    FirstName = y.FirstName,
                    LastName = y.LastName,
                    Status = y.Status
                    }).ToList();

                var allFriends = friends3.Select(y =>
                    new
                    {
                        PersonId = y.PersonId,
                        FirstName = y.FirstName,
                        LastName = y.LastName,
                        Status = y.Status
                    }).Take(10).ToList();

                if(friendName.Count() == 0)
                    return Json(allFriends, JsonRequestBehavior.AllowGet);
                else
                    return Json(searchedFriends, JsonRequestBehavior.AllowGet);
            }
        }



        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }


    }
}
