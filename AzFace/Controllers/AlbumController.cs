﻿using AzFace.Infrastructure;
using AzFace.Models;
using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AzFace.Controllers
{

    public class JSonAlbum
    {

        public int AlbumId { get; set; }
        public Nullable<int> PersonId { get; set; }
        public string Name { get; set; }
        public string CreationTime { get; set; }
        public List<int> PhotoIds { get; set; }
    }

    public class AlbumController : Controller
    {

        public ActionResult Index()
        {

            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(currentPersonId);
            ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(currentPersonId);
            ViewBag.Messages = FriendHelper.GetMessages(currentPersonId);
            ViewBag.MessageCount = FriendHelper.GetMessageCount(currentPersonId);

            using (EFPersonRepository db = new EFPersonRepository())
            {
                List<Album> albums = db.Albums.Where(x => x.PersonId == currentPersonId).ToList();
                return View(albums);
            }
        }



        [HttpPost]
        public JsonResult AsyncUpload(IEnumerable<HttpPostedFileBase> files, string albumName)
        {
            int count = 0;
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            Album album1 = null;
            if (files != null)
            {

                using (EFPersonRepository db = new EFPersonRepository())
                {
                    Album album = new Album { CreationTime = DateTime.Now, PersonId = currentPersonId, Name = albumName };
                    db.context.Albums.Add(album);
                    db.context.SaveChanges();
                }
                foreach (var file in files)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        using (EFPersonRepository db = new EFPersonRepository())
                        {

                            Photo photo = new Photo { ImageMimeType = file.ContentType, Image = new byte[file.ContentLength], PersonId = currentPersonId };
                            file.InputStream.Read(photo.Image, 0, file.ContentLength);

                            album1 = db.Albums.OrderByDescending(x => x.AlbumId).FirstOrDefault(x => x.PersonId == currentPersonId);
                            album1.Photos.Add(photo);
                            //db.context.SaveChanges();

                            Item item = new Item
                            {
                                ImageMimeType = file.ContentType,
                                ImageData = photo.Image,
                                PersonId = currentPersonId,
                                InitialPersonId = currentPersonId,
                                LikeNumber = 0,
                                ShareOption = "OnlyMe",
                                Text = albumName,
                                SharedTime = DateTime.Now
                            };
                            item.Photos.Add(photo);
                            Share share = new Share
                            {
                                ItemId = item.ItemId,
                                PersonId = currentPersonId
                            };
                            db.context.Shares.Add(share);
                            db.SaveItem(item);

                        }
                    }
                }

                using (EFPersonRepository db = new EFPersonRepository())
                {
                    Album album2 = db.Albums.FirstOrDefault(x => x.AlbumId == album1.AlbumId);

                    List<int> photoIds = new List<int>();
                    foreach (Photo photo2 in album2.Photos)
                    {
                        photoIds.Add(photo2.PhotoId);
                    }

                    JSonAlbum jsonAlbum = new JSonAlbum
                    {
                        AlbumId = album2.AlbumId,
                        CreationTime = album2.CreationTime.ToString(),
                        Name = album2.Name,
                        PersonId = album2.PersonId,
                        PhotoIds = photoIds
                    };

                    return Json(jsonAlbum, JsonRequestBehavior.AllowGet);
                }

            }
            return new JsonResult { Data = "Successfully " + count + " file(s) uploaded" };
        }


        [HttpPost]
        public JsonResult GetCarouselItems(int photoId, int albumId, string direction)
        {
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            if (albumId == 0 || photoId == 0)
                return null;

            using (EFPersonRepository db = new EFPersonRepository())
            {
                List<Photo> photos = db.Photos.Where(x => x.PhotoId >= photoId && x.AlbumId == albumId && x.PersonId == currentPersonId && x.ImageMimeType != null).ToList();

                if (direction == "left")
                    photos = db.Photos.Where(x => x.PhotoId < photoId && x.AlbumId == albumId && x.PersonId == currentPersonId && x.ImageMimeType != null).ToList();
                Photo photo = null;
                int? nextPhotoId = 0;

                if (photos.Count() > 0)
                    photo = photos[0];
                if (photos.Count() > 1)
                {
                    nextPhotoId = photos[1].PhotoId;
                }




                var carouselItem = new
                {
                    PersonId = photo.PersonId,
                    ImgMimeType = photo.ImageMimeType,
                    PersonName = photo.Person.FirstName + " " + photo.Person.LastName,
                    AlbumId = photo.AlbumId,
                    Text = photo.Item.Text,
                    Comments = photo.Item.Comments.Select(x => new { 
                        PersonId = x.PersonId, 
                        CommentTime = x.CommentTime.Value.ToShortDateString(), 
                        Text = x.Text, 
                        FirstName = x.Person.FirstName, 
                        LastName = x.Person.LastName }),

                    LikeNumber = photo.Item.LikeNumber,
                    InitialPersonId = photo.Item.InitialPersonId,
                    InitialPersonName = photo.Item.Person1.FirstName + " " + photo.Item.Person1.LastName,
                    SharedTime = photo.Item.SharedTime.ToString(),
                    ShareOption = photo.Item.ShareOption,
                    VideoPath = photo.Item.VideoPath,
                    NextPhotoId = nextPhotoId,
                    PhotoId = photo.PhotoId,
                    ItemId = photo.ItemId
                };
                return Json(carouselItem, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public FileContentResult GetPhoto(int photoId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int currentPersonId = Convert.ToInt32(Session["PersonId"]);

                Photo photo = db.Photos.FirstOrDefault(x => x.PhotoId == photoId && x.PersonId == currentPersonId);
                if (photo != null && photo.Image != null)
                {
                    return File(photo.Image, photo.ImageMimeType);
                }
                else
                {
                    return null;
                }
            }
        }

        public ActionResult GetAlbum(int albumId)
        {

            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(currentPersonId);
            ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(currentPersonId);
            ViewBag.Messages = FriendHelper.GetMessages(currentPersonId);
            ViewBag.MessageCount = FriendHelper.GetMessageCount(currentPersonId);

            using (EFPersonRepository db = new EFPersonRepository())
            {
                Album album = db.Albums.FirstOrDefault(x => x.AlbumId == albumId);
                return View(album);
            }
        }

        public JsonResult GetPeopleDataJson(String searchTerm)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                if (!String.IsNullOrWhiteSpace(searchTerm))
                {
                    int personId = Convert.ToInt32(Session["PersonId"]);
                    List<Person> people = db.People.Where(x => x.FirstName.StartsWith(searchTerm) || x.LastName.StartsWith(searchTerm)).ToList();
                    List<Person> people1 = people.Where(x => x.PersonId != personId).Select(p =>
                        new Person { PersonId = p.PersonId, LastName = p.LastName, FirstName = p.FirstName }).ToList();

                    return Json(people1, JsonRequestBehavior.AllowGet);
                }
                else
                    return null;
            }
        }

        public JsonResult GetAjaxImage(int albumId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int currentPersonId = Convert.ToInt32(Session["PersonId"]);
                Random r = new Random();

                Album album = db.Albums.FirstOrDefault(x => x.AlbumId == albumId && x.PersonId == currentPersonId);
                int index = r.Next(0, album.Photos.Count() - 1);
                Photo photo = album.Photos.ElementAtOrDefault(index);
                // hemin albomu tapir, sonra albomun sekillerinin arasinda random sekil goturur. sonra hemin sekilin id  sin qaytarir

                return Json(new { PhotoId = photo.PhotoId }, JsonRequestBehavior.AllowGet);
            }
        }




    }
}
