﻿using AzFace.Infrastructure;
using AzFace.Models;
using AzFace.Models.Connection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AzFace.Controllers
{

    public class MessageTest
    {
        public string Text;
        public string MessageTime;
        public string FirstName;
        public string LastName;
        public int? PersonId;
    }

    [Authorize]
    public class MessageController : Controller
    {

        public ActionResult Index(MessageViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Home", "Home", null);
            }

            int PersonId = model.PersonId;
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(currentPersonId);
            ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(currentPersonId);
            ViewBag.Messages = FriendHelper.GetMessages(currentPersonId);
            ViewBag.MessageCount = FriendHelper.GetMessageCount(currentPersonId);

            using (EFPersonRepository db = new EFPersonRepository())
            {
                Person senderPerson = db.People.FirstOrDefault(x => x.PersonId == PersonId);
                Person receiverPerson = db.People.FirstOrDefault(x => x.PersonId == currentPersonId);

                if (senderPerson == null)
                {
                    return RedirectToAction("Home","Home", null);
                }

                String senderName = senderPerson.FirstName + " " + senderPerson.LastName;
                String receiverName = receiverPerson.FirstName + " " + receiverPerson.LastName;

                List<Message> newMessages = db.Messages.Where(x =>
                    x.MessageFrom == PersonId && x.MessageTo == currentPersonId ||
                    x.MessageTo == PersonId && x.MessageFrom == currentPersonId
                    ).OrderBy(n => n.MessageTime).ToList();
                for (int i = 0; i < newMessages.Count(); i++)
                {
                    Message message = newMessages[i];
                    message.ReadTimes = message.ReadTimes + 1;
                }
                db.context.SaveChanges();

                MessageHelper messageHelper = new MessageHelper
                {
                    Messages = newMessages,
                    Sender = new Person
                    {
                        PersonId = PersonId,
                        FirstName = senderPerson.FirstName,
                        LastName = senderPerson.LastName
                    },
                    Receiver = new Person
                    {
                        PersonId = currentPersonId,
                        FirstName = receiverPerson.FirstName,
                        LastName = receiverPerson.LastName
                    }
                };
                return View(messageHelper);
            }
        }


        public JsonResult Messages(int personId)
        {
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(currentPersonId);
            ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(currentPersonId);
            ViewBag.Messages = FriendHelper.GetMessages(currentPersonId);
            ViewBag.MessageCount = FriendHelper.GetMessageCount(currentPersonId);

            using (EFPersonRepository db = new EFPersonRepository())
            {
                Person senderPerson = db.People.FirstOrDefault(x => x.PersonId == personId);
                Person receiverPerson = db.People.FirstOrDefault(x => x.PersonId == currentPersonId);

                String senderName = senderPerson.FirstName + " " + senderPerson.LastName;
                String receiverName = receiverPerson.FirstName + " " + receiverPerson.LastName;

                List<Message> newMessages = db.Messages.Where(x =>
                    x.MessageFrom == personId && x.MessageTo == currentPersonId ||
                    x.MessageTo == personId && x.MessageFrom == currentPersonId
                    ).OrderBy(n => n.MessageTime).ToList();
                for (int i = 0; i < newMessages.Count(); i++)
                {
                    Message message = newMessages[i];
                    message.ReadTimes = message.ReadTimes + 1;
                }
                db.context.SaveChanges();

                List<Message> jsonMessages = newMessages.Select(x => new Message
                {
                    MessageFrom = x.MessageFrom,
                    MessageTo = x.MessageTo,
                    MessageId = x.MessageId,
                    MessageTime = x.MessageTime,
                    Text = x.Text,
                    ReadTimes = x.ReadTimes,
                    Delivered = x.Delivered
                }).ToList();

                MessageHelper messageHelper = new MessageHelper
                {
                    Messages = jsonMessages,
                    Sender = new Person
                    {
                        PersonId = personId,
                        FirstName = senderPerson.FirstName,
                        LastName = senderPerson.LastName
                    },
                    Receiver = new Person
                    {
                        PersonId = currentPersonId,
                        FirstName = receiverPerson.FirstName,
                        LastName = receiverPerson.LastName
                    }
                };
                return Json(messageHelper, JsonRequestBehavior.AllowGet);
            }
        }



        public PartialViewResult PartialMessages(int personId)
        {
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            ViewBag.FriendRequestes = FriendHelper.GetFriendRequestes(currentPersonId);
            ViewBag.RequestCount = FriendHelper.GetFriendRequestCount(currentPersonId);
            ViewBag.Messages = FriendHelper.GetMessages(currentPersonId);
            ViewBag.MessageCount = FriendHelper.GetMessageCount(currentPersonId);

            using (EFPersonRepository db = new EFPersonRepository())
            {
                Person senderPerson = db.People.FirstOrDefault(x => x.PersonId == personId);
                Person receiverPerson = db.People.FirstOrDefault(x => x.PersonId == currentPersonId);

                String senderName = senderPerson.FirstName + " " + senderPerson.LastName;
                String receiverName = receiverPerson.FirstName + " " + receiverPerson.LastName;

                List<Message> newMessages = db.Messages.Where(x =>
                    x.MessageFrom == personId && x.MessageTo == currentPersonId ||
                    x.MessageTo == personId && x.MessageFrom == currentPersonId
                    ).OrderBy(n => n.MessageTime).ToList();
                for (int i = 0; i < newMessages.Count(); i++)
                {
                    Message message = newMessages[i];
                    message.ReadTimes = message.ReadTimes + 1;
                }
                db.context.SaveChanges();

                List<Message> jsonMessages = newMessages.Select(x => new Message
                {
                    MessageFrom = x.MessageFrom,
                    MessageTo = x.MessageTo,
                    MessageId = x.MessageId,
                    MessageTime = x.MessageTime,
                    Text = x.Text,
                    ReadTimes = x.ReadTimes,
                    TimeStamp = x.TimeStamp,
                    Delivered = x.Delivered,
                    FileName = x.FileName
                }).ToList();

                MessageHelper messageHelper = new MessageHelper
                {
                    Messages = jsonMessages,
                    Sender = new Person
                    {
                        PersonId = personId,
                        FirstName = senderPerson.FirstName,
                        LastName = senderPerson.LastName
                    },
                    Receiver = new Person
                    {
                        PersonId = currentPersonId,
                        FirstName = receiverPerson.FirstName,
                        LastName = receiverPerson.LastName
                    }
                };
                return PartialView(messageHelper);
            }
        }














        public JsonResult GetPeopleDataJson(String searchTerm)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                if (!String.IsNullOrWhiteSpace(searchTerm))
                {
                    int personId = Convert.ToInt32(Session["PersonId"]);
                    List<Person> people = db.People.Where(x => x.FirstName.StartsWith(searchTerm) || x.LastName.StartsWith(searchTerm)).ToList();
                    List<Person> people1 = people.Where(x => x.PersonId != personId).Select(p =>
                        new Person { PersonId = p.PersonId, LastName = p.LastName, FirstName = p.FirstName }).ToList();

                    return Json(people1, JsonRequestBehavior.AllowGet);
                }
                else
                    return null;
            }
        }

        [HttpPost]
        public JsonResult SetPeopleMessageJson(String messageReply, int receiverPersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                if (!String.IsNullOrWhiteSpace(messageReply) && receiverPersonId != 0)
                {
                    int senderPersonId = Convert.ToInt32(Session["PersonId"]);
                    Message message = new Message
                    {
                        MessageFrom = senderPersonId,
                        MessageTo = receiverPersonId,
                        MessageTime = DateTime.Now,
                        Text = messageReply,
                        ReadTimes = 0
                    };

                    db.context.Messages.Add(message);
                    db.context.SaveChanges();

                    return null;
                }
                else
                    return null;
            }
        }

        [HttpPost]
        public JsonResult GetNewMessages(int PersonId)
        {
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            using (EFPersonRepository db = new EFPersonRepository())
            {

                List<Message> newMessages = db.Messages.Where(x =>
                    (x.MessageFrom == PersonId && x.MessageTo == currentPersonId ||
                    x.MessageTo == PersonId && x.MessageFrom == currentPersonId) &&
                    (x.ReadTimes < 2 && x.MessageFrom != currentPersonId)
                    ).OrderBy(n => n.MessageTime).ToList();

                List<Message> message2 = newMessages.Select(x => new Message
                {
                    Text = x.Text,
                    MessageTime = x.MessageTime
                }).ToList();


                for (int i = 0; i < newMessages.Count(); i++)
                {
                    Message message = newMessages[i];
                    message.ReadTimes = message.ReadTimes + 1;
                }
                db.context.SaveChanges();

                return Json(message2, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetNewMessagesHeader()
        {
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            using (EFPersonRepository db = new EFPersonRepository())
            {

                List<Message> newMessages = db.Messages.Where(x =>
                    x.MessageTo == currentPersonId && x.ReadTimes < 1).ToList();

                var message2 = newMessages.Select(x => new
                {
                    Text = x.Text,
                    MessageTime = x.MessageTime,
                    FirstName = x.Person.FirstName,
                    LastName = x.Person.LastName,
                    PersonId = x.MessageFrom
                });

                for (int i = 0; i < newMessages.Count(); i++)
                {
                    Message message = newMessages[i];
                    message.ReadTimes = message.ReadTimes + 1;
                }
                db.context.SaveChanges();

                return Json(message2, JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult StartChat(int PersonId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {

                return null;
            }
        }


        [HttpPost]
        public JsonResult WriteMessage(int receiverPersonId, string text)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                int senderPersonId = Convert.ToInt32(Session["PersonId"]);
                Message message = new Message
                {
                    MessageFrom = senderPersonId,
                    MessageTo = receiverPersonId,
                    Text = text,
                    MessageTime = DateTime.Now,
                    ReadTimes = 0
                };

                db.SaveMessage(message);
                return Json("Message was sent", JsonRequestBehavior.AllowGet);
            }

        }


        public PartialViewResult GetLastAndNewMessages()
        {

            Thread.Sleep(1000);

            int currentPersonId = Convert.ToInt32(Session["PersonId"]);
            using (EFPersonRepository db = new EFPersonRepository())
            {
                
                List<Message> newMessages = db.Messages.Where(x => x.MessageTo == currentPersonId && x.ReadTimes < 2).GroupBy(y => y.MessageFrom).Select(n => n.OrderByDescending(p => p.MessageId).FirstOrDefault()).ToList();
                List<MessageTest> newMessages2 = newMessages.Select(x => new MessageTest
                {
                    Text = x.Text,
                    MessageTime = x.MessageTime.ToString(),
                    FirstName = x.Person.FirstName,
                    LastName = x.Person.LastName,
                    PersonId = x.MessageFrom
                }).ToList();


                var messages = db.Messages.Where(x =>
                    x.MessageTo == currentPersonId && x.ReadTimes >= 2).GroupBy(y => y.MessageFrom).Select(n => n.OrderByDescending(p => p.MessageId).FirstOrDefault()).Take(10).ToList();


                List<MessageTest> message2 = messages.Select(x => new MessageTest
                {
                    Text = x.Text,
                    MessageTime = x.MessageTime.ToString(),
                    FirstName = x.Person.FirstName,
                    LastName = x.Person.LastName,
                    PersonId = x.MessageFrom
                }).ToList();

                List<MessageTest> message3 = message2.Union(newMessages2).OrderByDescending(x => x.MessageTime).ToList();

                for (int i = 0; i < newMessages.Count(); i++)
                {
                    Message message = newMessages[i];
                    message.ReadTimes = message.ReadTimes + 1;
                }
                db.context.SaveChanges();


                return PartialView(message3);
            }
        }




        public JsonResult UploadFile(int receiverId)
        {
            var file = Request.Files["dialogFile"];
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);

            string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string subPath = "~/PdfFile/" + currentPersonId + "-" + receiverId;
            bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));
            if (!exists)
                System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

            string fileName = timeStamp + Path.GetFileName(file.FileName);

            string filePath = Path.Combine(Server.MapPath(subPath), fileName); // gelir burda yazir folder e.
            file.SaveAs(filePath);                                              // folder in de path in ozum gonderenin ve qebul edenin
                                                                                      //id lerine esasen duzeldirem.sonra falyi yaziram ora.
            return Json(new { FileName = fileName, ReceiverId = receiverId});  //geri qaytariram filein adin ve receiver person id.
        }



        public FileResult DownloadFile(string fileName, int receiverId)
        {
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);

            string subPath = "~/PdfFile/" + currentPersonId + "-" + receiverId;
            string filePath = Path.Combine(Server.MapPath(subPath), fileName);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public FileResult DownloadFileReceiver(string fileName, int receiverId)
        {
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);

            string subPath = "~/PdfFile/" + receiverId + "-" + currentPersonId;
            string filePath = Path.Combine(Server.MapPath(subPath), fileName);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public JsonResult DeleteFile(string fileName, int receiverId)
        {
            
            int currentPersonId = Convert.ToInt32(Session["PersonId"]);

            string subPath = "~/PdfFile/" + currentPersonId + "-" + receiverId;
            string filePath = Path.Combine(Server.MapPath(subPath), fileName);
            try
            {
                using (EFPersonRepository db = new EFPersonRepository())
                {

                }
                System.IO.File.Delete(filePath);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }






    }
}
