﻿using AzFace.CustomResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AzFace.Controllers
{
    public class VideoController : Controller
    {
        //
        // GET: /Video/

        public ActionResult Index(int personId, string fileName)
        {
            return new VideoResult(personId, fileName); 

        }

    }
}
