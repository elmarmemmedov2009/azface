namespace AzFace.Migrations
{
    using AzFace.Infrastructure;
    using AzFace.Models;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    internal sealed class Configuration : DbMigrationsConfiguration<AzFace.Infrastructure.AppIdentityDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "AzFace.Infrastructure.AppIdentityDbContext";
        }

        protected override void Seed(AzFace.Infrastructure.AppIdentityDbContext context)
        {
            //AppUserManager userMgr = new AppUserManager(new UserStore<AppUser>(context));
            //AppRoleManager roleMgr = new AppRoleManager(new RoleStore<AppRole>(context));
            //string roleName = "Administrators";
            //string userName = "Admin";
            //string password = "MySecret1";
            //string email = "admin@example.com";
            //bool x = await roleMgr.RoleExistsAsync(roleName);
            //if (!x )
            //{
            //    await roleMgr.CreateAsync(new AppRole(roleName));
            //}
            //AppUser user = await userMgr.FindByNameAsync(userName);
            //if (user == null)
            //{
            //    await userMgr.CreateAsync(new AppUser { UserName = userName, Email = email },
            //    password);
            //    user = await userMgr.FindByNameAsync(userName);
            //}

            //bool y = await userMgr.IsInRoleAsync(user.Id, roleName);
            //if (!y)
            //{
            //   await userMgr.AddToRoleAsync(user.Id, roleName);
            //}
            ////foreach (AppUser dbUser in userMgr.Users)
            ////{
            ////    dbUser. = Cities.PARIS;
            ////}
            //context.SaveChanges();
        }
    }
}
