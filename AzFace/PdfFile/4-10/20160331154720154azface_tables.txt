CREATE TABLE [dbo].[Album] (
    [AlbumId]      INT           IDENTITY (1, 1) NOT NULL,
    [PersonId]     INT           NULL,
    [Name]         NVARCHAR (50) NULL,
    [CreationTime] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([AlbumId] ASC),
    CONSTRAINT [FK_Album_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId])
);





CREATE TABLE [dbo].[Comment] (
    [CommentId]   INT            IDENTITY (1, 1) NOT NULL,
    [Text]        NVARCHAR (400) NULL,
    [ItemId]      INT            NULL,
    [PersonId]    INT            NULL,
    [CommentTime] DATETIME       NULL,
    CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED ([CommentId] ASC),
    CONSTRAINT [FK_Comment_ToTable] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Item] ([ItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_Comment_ToTable_1] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Comment_ToTable]
    ON [dbo].[Comment]([ItemId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Comment_ToTable_1]
    ON [dbo].[Comment]([PersonId] ASC);







CREATE TABLE [dbo].[CommentLike] (
    [CommentLikeId] INT IDENTITY (1, 1) NOT NULL,
    [PersonId]      INT NULL,
    [CommentId]     INT NULL,
    CONSTRAINT [PK_CommentLikes] PRIMARY KEY CLUSTERED ([CommentLikeId] ASC),
    CONSTRAINT [FK_CommentLike_ToTable_1] FOREIGN KEY ([CommentId]) REFERENCES [dbo].[Comment] ([CommentId]) ON DELETE CASCADE,
    CONSTRAINT [FK_CommentLike_ToTable] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_CommentLike_ToTable_1]
    ON [dbo].[CommentLike]([CommentId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_CommentLike_ToTable]
    ON [dbo].[CommentLike]([PersonId] ASC);







CREATE TABLE [dbo].[CommentReply] (
    [ComentReplyId] INT IDENTITY (1, 1) NOT NULL,
    [CommentId]     INT NULL,
    [ReplyId]       INT NULL,
    PRIMARY KEY CLUSTERED ([ComentReplyId] ASC),
    CONSTRAINT [FK_CommentReply_ToTable] FOREIGN KEY ([CommentId]) REFERENCES [dbo].[Comment] ([CommentId]) ON DELETE CASCADE,
    CONSTRAINT [FK_CommentReply_ToTable_1] FOREIGN KEY ([ReplyId]) REFERENCES [dbo].[Comment] ([CommentId])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_CommentReply_ToTable]
    ON [dbo].[CommentReply]([CommentId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_CommentReply_ToTable_1]
    ON [dbo].[CommentReply]([ReplyId] ASC);










CREATE TABLE [dbo].[Friend] (
    [FriendId]  INT          IDENTITY (1, 1) NOT NULL,
    [PersonId1] INT          NULL,
    [PersonId2] INT          NULL,
    [Status]    NVARCHAR (5) NULL,
    CONSTRAINT [PK_Friends] PRIMARY KEY CLUSTERED ([FriendId] ASC),
    CONSTRAINT [FK_Friend_ToTable] FOREIGN KEY ([PersonId1]) REFERENCES [dbo].[Person] ([PersonId]),
    CONSTRAINT [FK_Friend_ToTable_1] FOREIGN KEY ([PersonId2]) REFERENCES [dbo].[Person] ([PersonId])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Friend_ToTable]
    ON [dbo].[Friend]([PersonId1] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Friend_ToTable_1]
    ON [dbo].[Friend]([PersonId2] ASC);










CREATE TABLE [dbo].[Item] (
    [ItemId]          INT             IDENTITY (1, 1) NOT NULL,
    [Text]            NVARCHAR (1000) NULL,
    [ImageData]       VARBINARY (MAX) NULL,
    [ImageMimeType]   NVARCHAR (50)   NULL,
    [PersonId]        INT             NULL,
    [SharedTime]      DATETIME        NULL,
    [ShareOption]     NVARCHAR (10)   NULL,
    [LikeNumber]      INT             NULL,
    [InitialPersonId] INT             NULL,
    [VideoPath]       NVARCHAR (100)  NULL,
    CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED ([ItemId] ASC),
    CONSTRAINT [FK_Item_ToTable] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]),
    CONSTRAINT [FK_Item_ToTable_1] FOREIGN KEY ([InitialPersonId]) REFERENCES [dbo].[Person] ([PersonId])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Item_ToTable]
    ON [dbo].[Item]([PersonId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Item_ToTable_1]
    ON [dbo].[Item]([InitialPersonId] ASC);












CREATE TABLE [dbo].[ItemLike] (
    [ItemLikeId] INT IDENTITY (1, 1) NOT NULL,
    [ItemId]     INT NULL,
    [PersonId]   INT NULL,
    PRIMARY KEY CLUSTERED ([ItemLikeId] ASC),
    CONSTRAINT [AK_TransactionID] UNIQUE NONCLUSTERED ([ItemId] ASC, [PersonId] ASC),
    CONSTRAINT [FK_ItemLike_ToTable] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Item] ([ItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ItemLike_ToTable_1] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId])
);











CREATE TABLE [dbo].[Message] (
    [MessageId]   INT             IDENTITY (1, 1) NOT NULL,
    [MessageFrom] INT             NULL,
    [MessageTo]   INT             NULL,
    [MessageTime] DATETIME        NULL,
    [Text]        NVARCHAR (1500) NULL,
    [Status]      NVARCHAR (10)   NULL,
    [ReadTimes]   INT             NULL,
    CONSTRAINT [PK_Messages] PRIMARY KEY CLUSTERED ([MessageId] ASC),
    CONSTRAINT [FK_Message_ToTable] FOREIGN KEY ([MessageFrom]) REFERENCES [dbo].[Person] ([PersonId]),
    CONSTRAINT [FK_Message_ToTable_1] FOREIGN KEY ([MessageTo]) REFERENCES [dbo].[Person] ([PersonId])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Message_ToTable]
    ON [dbo].[Message]([MessageFrom] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Message_ToTable_1]
    ON [dbo].[Message]([MessageTo] ASC);







CREATE TABLE [dbo].[Person] (
    [PersonId]  INT            IDENTITY (1, 1) NOT NULL,
    [UserId]    NVARCHAR (128) NULL,
    [FirstName] NVARCHAR (50)  NULL,
    [LastName]  NVARCHAR (50)  NULL,
    [Birthday]  DATETIME       NULL,
    [Status]    NVARCHAR (50)  NULL,
    CONSTRAINT [PK_People] PRIMARY KEY CLUSTERED ([PersonId] ASC),
    CONSTRAINT [FK_Person_ToTable_1] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Person_ToTable_1]
    ON [dbo].[Person]([UserId] ASC);














CREATE TABLE [dbo].[Photo] (
    [PhotoId]       INT             IDENTITY (1, 1) NOT NULL,
    [Image]         VARBINARY (MAX) NULL,
    [ImageMimeType] VARCHAR (50)    NULL,
    [PersonId]      INT             NOT NULL,
    [ImageType]     VARCHAR (50)    NULL,
    [AlbumId]       INT             NULL,
    [ItemId]        INT             NULL,
    CONSTRAINT [PK_Photos] PRIMARY KEY CLUSTERED ([PhotoId] ASC),
    CONSTRAINT [FK_Photo_Album] FOREIGN KEY ([AlbumId]) REFERENCES [dbo].[Album] ([AlbumId]) ON DELETE CASCADE,
    CONSTRAINT [FK_Photo_Item] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Item] ([ItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_Photo_ToTable] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Photo_ToTable]
    ON [dbo].[Photo]([PersonId] ASC);












CREATE TABLE [dbo].[Share] (
    [ShareId]  INT IDENTITY (1, 1) NOT NULL,
    [ItemId]   INT NULL,
    [PersonId] INT NULL,
    CONSTRAINT [PK_Shares] PRIMARY KEY CLUSTERED ([ShareId] ASC),
    CONSTRAINT [FK_Share_ToTable] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Item] ([ItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_Share_ToTable_1] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Share_ToTable]
    ON [dbo].[Share]([ItemId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Share_ToTable_1]
    ON [dbo].[Share]([PersonId] ASC);

