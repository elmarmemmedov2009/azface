﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzFace.Models
{
    public class AppUser : IdentityUser
    {
        // additional properties will go here
        public Guid PasswordResetToken { get; set; }
        public Nullable<DateTime> PasswordResetTokenExpire { get; set; }
    }
}