//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AzFace.Models.Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class Person
    {
        public Person()
        {
            this.Comments = new HashSet<Comment>();
            this.CommentLikes = new HashSet<CommentLike>();
            this.Friends = new HashSet<Friend>();
            this.Friends1 = new HashSet<Friend>();
            this.Items = new HashSet<Item>();
            this.Items1 = new HashSet<Item>();
            this.ItemLikes = new HashSet<ItemLike>();
            this.Messages = new HashSet<Message>();
            this.Messages1 = new HashSet<Message>();
            this.Photos = new HashSet<Photo>();
            this.Shares = new HashSet<Share>();
            this.Albums = new HashSet<Album>();
        }
    
        public int PersonId { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public string Status { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<CommentLike> CommentLikes { get; set; }
        public virtual ICollection<Friend> Friends { get; set; }
        public virtual ICollection<Friend> Friends1 { get; set; }
        public virtual ICollection<Item> Items { get; set; }
        public virtual ICollection<Item> Items1 { get; set; }
        public virtual ICollection<ItemLike> ItemLikes { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Message> Messages1 { get; set; }
        public virtual ICollection<Photo> Photos { get; set; }
        public virtual ICollection<Share> Shares { get; set; }
        public virtual ICollection<Album> Albums { get; set; }
    }
}
