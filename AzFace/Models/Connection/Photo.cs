//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AzFace.Models.Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class Photo
    {
        public int PhotoId { get; set; }
        public byte[] Image { get; set; }
        public string ImageMimeType { get; set; }
        public int PersonId { get; set; }
        public string ImageType { get; set; }
        public Nullable<int> AlbumId { get; set; }
        public Nullable<int> ItemId { get; set; }
    
        public virtual Person Person { get; set; }
        public virtual Album Album { get; set; }
        public virtual Item Item { get; set; }
    }
}
