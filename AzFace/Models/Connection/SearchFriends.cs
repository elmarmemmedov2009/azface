﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzFace.Models.Connection
{
    public class SearchFriends
    {
        public List<Person> persons;
        public List<Friend> friends;
    }
}