//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AzFace.Models.Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class Friend
    {
        public int FriendId { get; set; }
        public Nullable<int> PersonId1 { get; set; }
        public Nullable<int> PersonId2 { get; set; }
        public string Status { get; set; }
    
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
