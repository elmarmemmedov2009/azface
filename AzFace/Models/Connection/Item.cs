//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AzFace.Models.Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class Item
    {
        public Item()
        {
            this.Comments = new HashSet<Comment>();
            this.ItemLikes = new HashSet<ItemLike>();
            this.Shares = new HashSet<Share>();
            this.Photos = new HashSet<Photo>();
        }
    
        public int ItemId { get; set; }
        public string Text { get; set; }
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
        public Nullable<int> PersonId { get; set; }
        public Nullable<System.DateTime> SharedTime { get; set; }
        public string ShareOption { get; set; }
        public Nullable<int> LikeNumber { get; set; }
        public Nullable<int> InitialPersonId { get; set; }
        public string VideoPath { get; set; }
    
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<ItemLike> ItemLikes { get; set; }
        public virtual ICollection<Share> Shares { get; set; }
        public virtual ICollection<Photo> Photos { get; set; }
    }
}
