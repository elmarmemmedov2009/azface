﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AzFace.Models
{
    public class MessageViewModel
    {
        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "Please enter a valid number")]
        public int PersonId { get; set; }
    }
}