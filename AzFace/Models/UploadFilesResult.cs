﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzFace.Models
{
    public class ViewDataUploadFilesResult
    {
        public string Name { get; set; }
        public int Length { get; set; }
        public string Type { get; set; }
        public int AlbumId { get; set; }
    }
}