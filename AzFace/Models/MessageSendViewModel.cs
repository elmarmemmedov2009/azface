﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AzFace.Models
{
    public class MessageSendViewModel
    {
        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "Please enter a valid number")]
        public int senderPersonId;

        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "Please enter a valid number")]
        public int receiverPersonId;

        [Required]
        public string text;
    }
}