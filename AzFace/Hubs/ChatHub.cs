﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using AzFace.Infrastructure;
using AzFace.Models.Connection;
using System.Web.Mvc;
using AzFace.Models;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using System.Dynamic;
using System.ComponentModel;
using System.Web.Providers.Entities;
using Microsoft.Security.Application;
using System.Collections;



namespace AzFace.Hubs
{
    [System.Web.Mvc.Authorize]
    public class ChatHub : Hub 
    {
        public void SendMessage(int senderPersonId, int receiverPersonId, string text, string  fileName)
        {


            // sender ve receiver preson id lere gore personlari teyin eliyin onlara mesja gonderirem.
            string encodedText = HttpUtility.HtmlEncode(text);
            using (EFPersonRepository db = new EFPersonRepository())
            {
                DateTime dt = DateTime.Now;
                string timeStamp = GetTimestamp(dt);

                if (!String.IsNullOrWhiteSpace(encodedText) && receiverPersonId != 0)
                {
                    Message message = new Message
                    {
                        MessageFrom = senderPersonId,
                        MessageTo = receiverPersonId,
                        MessageTime = dt,
                        Text = encodedText,
                        ReadTimes = 1,
                        Delivered = false,
                        Read = false,
                        TimeStamp = timeStamp,
                        FileName = fileName

                    }; // bu qeder.
                    
                    db.SaveMessage(message);

                    Person receiverPeson = db.People.FirstOrDefault(x => x.PersonId == receiverPersonId);
                    Person senderPerson = db.People.FirstOrDefault(x => x.PersonId == senderPersonId);

                    String senderPersonName = senderPerson.FirstName + " " + senderPerson.LastName;

                    String receiverUserId = receiverPeson.UserId;
                    String receiverUserName = db.AspNetUsers.FirstOrDefault(x => x.Id == receiverUserId).UserName;
                    String senderUserName = Context.User.Identity.Name;

                    string text1 = encodedText.Replace(":)", "<img width='35' height ='35'src='/Content/Smiley/happy.png' alt='Happy!' />");
                    text1 = text1.Replace(":(", "<img width='35' height ='35' src='/Content/Smiley/unhappy.png' alt='Unhappy!' />");
                    text1 = text1.Replace(":D", "<img width='35' height ='35' src='/Content/Smiley/moreHappy.png' alt='more happy!' />");
                    text1 = text1.Replace(";)", "<img width='35' height ='35' src='/Content/Smiley/wink.png' alt='wink!' />");

                    
                    // Clients signalr in classidi. verilen username lere mesaj gonderir. // bu PublishMessage gedir, home a 
                    Clients.Users(new List<string> { senderUserName, receiverUserName }).PublishMessage(senderPersonId, receiverPersonId, text1, senderPerson.FirstName, timeStamp, fileName);
                    Clients.Users(new List<string> { receiverUserName }).PublishHeaderMessage(senderPersonId, senderPersonName, receiverPersonId, text1);


                }
            }

        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssfff");
        }

        public override Task OnConnected()
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                var name = Context.User.Identity.Name;


                if (name != "")
                {
                    String userId = db.AspNetUsers.FirstOrDefault(x => x.UserName == name).Id;

                    Person person = db.People.FirstOrDefault(x => x.UserId == userId);
                    person.Status = "On";
                    int personId = person.PersonId;
                    db.context.SaveChanges();

                    Clients.All.PublishOnline(name, personId);

                    List<Message> nonDeliveredMessages1 = person.Messages.Where(x => x.MessageTo == personId && x.Delivered == false).ToList();
                    List<Message> nonDeliveredMessages2 = person.Messages1.Where(x => x.MessageTo == personId && x.Delivered == false).ToList();

                    List<Message> nonDeliveredMessages = nonDeliveredMessages1.Union(nonDeliveredMessages2).ToList();
                    List<String> timeStamps = nonDeliveredMessages.Select(x => x.TimeStamp).ToList();
                    List<String> nonDeliveredUsers = nonDeliveredMessages.Select(x => x.Person.AspNetUser.UserName).ToList();

                    Clients.Users(nonDeliveredUsers).MessageDeliveredOnConnect(personId, timeStamps);

                    foreach (Message message in nonDeliveredMessages)
                    {
                        message.Delivered = true;
                    }
                    db.context.SaveChanges();

                    return base.OnConnected();
                }
                else
                {
                    return null;
                }
            }
        }

        public override Task OnReconnected()
        {
            var name = Context.User.Identity.Name;
            Clients.All.PublishReonline(name);
            return base.OnReconnected();
        }


        public override Task OnDisconnected(bool stopCalled)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                var name = Context.User.Identity.Name;



                if (name != "")
                {
                    String userId = db.AspNetUsers.FirstOrDefault(x => x.UserName == name).Id;

                    Person person = db.People.FirstOrDefault(x => x.UserId == userId);
                    person.Status = "off";
                    int personId = person.PersonId;
                    db.context.SaveChanges();

                    Clients.All.PublishOffline(name, personId);
                    return base.OnDisconnected(stopCalled);
                }
                else
                {
                    return null;
                }
            }
        }


        public void PersonTyping(string name, int personId)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {
                Person person = db.People.FirstOrDefault(x => x.PersonId == personId);
                string userName = person.AspNetUser.UserName;
                Clients.User(userName).IsTyping(name);
            }
        }

        public void messageDelivered(int senderPersonId, string timeStamp)
        {
            using (EFPersonRepository db = new EFPersonRepository())
            {   // kim gonderirse, onun tapiram db dan id ye esasen. username i tapiram, sonra sender e, geri gondrirem ki mesaj delivered,
                Person person = db.People.FirstOrDefault(x => x.PersonId == senderPersonId);
                string userName = person.AspNetUser.UserName;
                Clients.User(userName).DeliveryConfirmed(timeStamp);
            }
        }

        

        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

    }
}